﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Web.Mvc;
using UrlRespone_ASP.NET_4._6.Models;

namespace UrlRespone_ASP.NET_4._6.Controllers
{


	public class HomeController : Controller
	{
		ProgramContext _context = new ProgramContext();


		[HttpGet]
		public ActionResult Index()
		{


			ViewBag.JsonList = JsonConvert.SerializeObject(_context.Urls.ToList());

			return View();
		}


		public JsonResult AddUrl(UrlJson data)
		{
			List<Url> urls = new List<Url>();
			List<SiteMapViewModel> siteMapItems = new List<SiteMapViewModel>();
			try
			{
				var urlToAdd = new Url();
				urlToAdd.UrlString = data.Value;
				urlToAdd.DateModified = DateTime.Now;
				_context.Urls.Add(urlToAdd);
				_context.SaveChanges();

				int idToParse = urlToAdd.Id;

				TimeHelper timeHelper = new TimeHelper();

				urls = timeHelper.SitemapsCalculating(data.Value, data.Number, idToParse, out siteMapItems);
			
			}
			catch (Exception)
			{
				urls = _context.Urls.ToList();
				return Json(new { urlsList = urls, error = "error"});
			}

			return Json(new { urlsList = urls, siteMapItems });
		}
	}
}
