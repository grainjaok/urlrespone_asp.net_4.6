﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace UrlRespone_ASP.NET_4._6.Models
{

	public class TimeHelper
	{
		public List<Url> SitemapsCalculating(string url, int? numberOfSites, int id, out List<SiteMapViewModel> siteMapItems)
		{
			List<Url> urls = new List<Url>();

			List<string> parsedSitemap = ParseSitemapFile(url);

			SiteMap siteMapToAdd = new SiteMap();

			using (ProgramContext context = new ProgramContext())
			{
				if (numberOfSites.HasValue)
				{
					context.SiteMaps.AddRange(parsedSitemap.Take(numberOfSites.Value).Select(x => new SiteMap() { NodeUrl = x, ResponeTime = RequestTime(x), UrlId = id }));
				}
				else
				{
					context.SiteMaps.AddRange(parsedSitemap.Select(x => new SiteMap() { NodeUrl = x, ResponeTime = RequestTime(x), UrlId = id }));
				}

				context.SaveChanges();

				var urlToUpdate = context.Urls.Find(id);
				if (urlToUpdate != null)
				{
					urlToUpdate.MinResponeTime = context.SiteMaps.Where(x => x.UrlId.Equals(id)).Min(x => (int)x.ResponeTime);
					urlToUpdate.MaxResponeTime = context.SiteMaps.Where(x => x.UrlId.Equals(id)).Max(x => (int)x.ResponeTime);
					context.Entry(urlToUpdate).State = EntityState.Modified;
				}

				var siteMaps = from u in context.Urls
							   join s in context.SiteMaps
							   on u.Id equals s.UrlId
							   where u.Id == id
							   select new SiteMapViewModel()
							   {
								   NodeUrl = s.NodeUrl,
								   ResponeTime = s.ResponeTime
							   };
				siteMapItems = siteMaps.OrderByDescending(x => x.ResponeTime).ToList();

				context.SaveChanges();
				urls = context.Urls.ToList();
			}

			return urls;
		}

		private List<string> ParseSitemapFile(string url)
		{
			StringBuilder _url = new StringBuilder();
			_url.Append(url + "/sitemap.xml");
			XmlDocument rssXmlDoc = new XmlDocument();

			rssXmlDoc.Load(_url.ToString());

			List<string> sitemapContent = new List<string>();

			foreach (XmlNode topNode in rssXmlDoc.ChildNodes)
			{
				if (topNode.Name.ToLower() == "urlset")
				{
					XmlNamespaceManager nsmgr = new XmlNamespaceManager(rssXmlDoc.NameTable);
					nsmgr.AddNamespace("ns", topNode.NamespaceURI);

					XmlNodeList urlNodes = topNode.ChildNodes;
					foreach (XmlNode urlNode in urlNodes)
					{
						XmlNode locNode = urlNode.SelectSingleNode("ns:loc", nsmgr);
						string link = locNode != null ? locNode.InnerText : "";

						sitemapContent.Add(link);
					}
				}
			}

			return sitemapContent;
		}


		private int RequestTime(string url)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			Stopwatch timer = new Stopwatch();

			timer.Start();

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			response.Close();

			timer.Stop();

			TimeSpan timeTaken = timer.Elapsed;
			return timeTaken.Milliseconds;
		}


	}
}
