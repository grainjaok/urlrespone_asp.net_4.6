﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace UrlRespone_ASP.NET_4._6.Models
{
    public class ProgramContext : DbContext
	{
		public ProgramContext()
            :base("DbConnection")
        { }
		public DbSet<Url> Urls { get; set; }

		public DbSet<SiteMap> SiteMaps { get; set; }
	}
}
