﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UrlRespone_ASP.NET_4._6.Models
{
	public class SiteMapViewModel
	{
		public string NodeUrl { get; set; }
		public int? ResponeTime { get; set; }
	}

}