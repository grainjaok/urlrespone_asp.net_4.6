﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UrlRespone_ASP.NET_4._6.Models
{
	public class Url
	{

		public int Id { get; set; }

		public string UrlString { get; set; }

		public DateTime DateModified { get; set; }

		public int MinResponeTime { get; set; }

		public int MaxResponeTime { get; set; }

	}

	public class UrlJson
	{
		public string Value { get; set; }
		public int? Number { get; set; }
	}

}
