﻿
var app = app || angular.module('app', ["chart.js"]);

app.controller("UrlController", function ($scope, $http, $window, $timeout) {
    
    $scope.init = function (addUrl, listUrl) {
        $scope.addNewUrl = addUrl;
        $scope.jsonListUrls = listUrl;
    };
    $scope.loader = {
        loading: false,
    };
    $scope.$on('chart-create', function (event, chart) {
        chart.chart.options = $scope.options;
    });
    
    $scope.sendUrl = function () {
        $scope.loader.loading = true;
        $scope.gifStatus = true;
        var stringData = JSON.stringify({ "value": $scope.name, "number" : $scope.number });
        $http.post($scope.addNewUrl, stringData, [({ 'Content-Type': 'application/json' })]).success(function (data) {
            $scope.jsonListUrls = data.urlsList;
            $scope.loader.loading = false;
            $scope.name = "";
            $scope.number = "";
            
            if (data.error == "error") {
                $scope.data = [];
                $scope.labels = [];
                alert("You entered wrong URL or this web site dosent contain a sitemap, please try again");
            } else {
                $scope.siteMaps = data.siteMapItems;

                $scope.options = {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
                var arrData = new Array();
                var arrLabels = new Array();
                $.map($scope.siteMaps, function (item) {
                    arrData.push(item.ResponeTime);
                    arrLabels.push(item.NodeUrl);
                })
                $scope.data = [];
                $scope.labels = [];
                $scope.data.push(arrData);

                for (var i = 0; i < arrLabels.length; i++) {
                    $scope.labels.push(arrLabels[i]);
                }

            };
   
           
        })
    };
})