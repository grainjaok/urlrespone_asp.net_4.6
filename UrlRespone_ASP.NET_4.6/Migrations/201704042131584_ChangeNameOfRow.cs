namespace UrlRespone_ASP.NET_4._6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeNameOfRow : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Urls", "MinResponeTime", c => c.Int(nullable: false));
            DropColumn("dbo.Urls", "MixResponeTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Urls", "MixResponeTime", c => c.Int(nullable: false));
            DropColumn("dbo.Urls", "MinResponeTime");
        }
    }
}
