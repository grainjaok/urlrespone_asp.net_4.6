namespace UrlRespone_ASP.NET_4._6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SiteMaps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NodeUrl = c.String(),
                        UrlId = c.Int(nullable: false),
                        ResponeTime = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Urls", "MixResponeTime", c => c.Int(nullable: false));
            AddColumn("dbo.Urls", "MaxResponeTime", c => c.Int(nullable: false));
            DropColumn("dbo.Urls", "Pending");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Urls", "Pending", c => c.Int(nullable: false));
            DropColumn("dbo.Urls", "MaxResponeTime");
            DropColumn("dbo.Urls", "MixResponeTime");
            DropTable("dbo.SiteMaps");
        }
    }
}
