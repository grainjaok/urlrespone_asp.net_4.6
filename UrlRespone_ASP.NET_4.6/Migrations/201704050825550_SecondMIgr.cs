namespace UrlRespone_ASP.NET_4._6.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SecondMIgr : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SiteMaps", "ResponeTime", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SiteMaps", "ResponeTime", c => c.Int(nullable: false));
        }
    }
}
