namespace UrlRespone_ASP.NET_4._6.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<UrlRespone_ASP.NET_4._6.Models.ProgramContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "UrlRespone_ASP.NET_4._6.Models.ProgramContext";
        }

        protected override void Seed(UrlRespone_ASP.NET_4._6.Models.ProgramContext context)
        {
           
        }
    }
}
